//
//  IDApp.swift
//  ID
//
//  Created by admin on 25.03.2022.
//

import SwiftUI

@main
struct IDApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
