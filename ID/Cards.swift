//
//  Cards.swift
//  ID
//
//  Created by admin on 25.03.2022.
//

import SwiftUI

struct Cards: Identifiable {
    var id = UUID()
    var name: String
    var phone: String
    var headline: String
    
    var imageName: String { return name }
}

#if DEBUG
let contact = [Cards(name: "Evgeniy Minaev", phone: "123", headline: "Friend"),
               Cards(name: "Mama", phone: "456", headline: "Family"),
               Cards(name: "Andrey Leonidovich", phone: "789", headline: "Doctor"),
               Cards(name: "Nastya", phone: "013", headline: "Colleague"),
               Cards(name: "Emergency", phone: "112", headline: "Other")]
#endif
