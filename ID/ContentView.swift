//
//  ContentView.swift
//  ID
//
//  Created by admin on 25.03.2022.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        TabView {
            Greeting()
                .tabItem {
                    Image(systemName: "house")
                    Text("Home")
                }
            CardsInfo(dataPeoples: contact)
                .tabItem {
                    Image(systemName: "person.text.rectangle")
                    Text("Contacts")
                }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
