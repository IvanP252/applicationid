//
//  CardDetails.swift
//  ID
//
//  Created by admin on 25.03.2022.
//

import SwiftUI

struct CardDetails: View {
    var name: String
    var headline: String
    var phone: String
    
    var body: some View {
        VStack {
            Image(name)
                .resizable()
                .aspectRatio(contentMode: .fit)
                .clipShape(Rectangle())
                .overlay(Rectangle().stroke(Color .yellow, lineWidth: 3))
                .shadow(radius: 10)
            Text(name)
                .font(.title)
            Text(headline)
                .font(.subheadline)
            Divider()
            Text(phone)
        }.padding().navigationBarTitle(Text(name), displayMode: .inline)
    }
}


struct CardDetails_Previews: PreviewProvider {
    static var previews: some View {
        CardDetails(name: "Nastya", headline: "Colleague", phone: "013")
    }
}
