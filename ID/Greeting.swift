//
//  Greeting.swift
//  ID
//
//  Created by admin on 25.03.2022.
//

import SwiftUI

struct Greeting: View {
    var body: some View {
        Image("ID")
            .resizable()
            .aspectRatio(contentMode: .fit)
    }
}

struct Greeting_Previews: PreviewProvider {
    static var previews: some View {
        Greeting()
    }
}
