//
//  CardsInfo.swift
//  ID
//
//  Created by admin on 25.03.2022.
//

import SwiftUI

struct CardsInfo: View {
    var dataPeoples: [Cards] = []
    
    var body: some View {
        NavigationView {
            List(dataPeoples) { people in
                DataCell(people: people)
            }.navigationBarTitle(Text("Contact"))
        }
    }
}

//#if DEBUG
struct CardsInfo_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            CardsInfo(dataPeoples: contact)
        }
    }
}
//#endif

struct DataCell: View {
    
    let people: Cards
    
    var body: some View {
        return NavigationLink(destination: CardDetails(name: people.name, headline: people.headline, phone: people.phone)) {
            Image(people.imageName)
                .resizable()
                .aspectRatio(contentMode: .fit)
            VStack(alignment: .leading) {
                Text(people.name)
                    .font(.subheadline)
                    .foregroundColor(.black)
                    .bold()
                Text(people.headline)
                    .font(.subheadline)
                    .foregroundColor(Color.gray)
            }
            
        }
    }
}
